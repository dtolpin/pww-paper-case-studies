"""Progressive Window Widening"""

def PWW(search, stream, t, L_max):
    """A simplified implementation of Progressive Window Widening
    for empirical evaluation purposes.
    Accepts:
        search --- the search function
        stream --- the input stream 
        t      --- initial batch duration
        L_max  --- maximum subsequence length
    Returns time at each a pattern was detected, or None if no pattern.
    """
    for i in range(1, len(stream) + L_max + 1):
        j = 1
        # This simplified implementation may be made sequential,
        # but not the actual implementation which must be parallel
        # or use cleverly crafted lazy streams
        while i % (j * t) == 0 and i >= 2 * j * t:
            window = stream[i - 2 * j * t: i]
            if(len(window) > 4 * L_max):
                # Remove parts of window which cannot contain a new pattern
                window = window[: L_max] + \
                         window[len(window) / 2 - L_max 
                                : len(window) / 2 + L_max] + \
                         window[-L_max :]
            if search(window):
                    return i
            j = j * 2
