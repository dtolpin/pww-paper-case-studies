"""Support code for PWW case studies.
"""

import unittest
from . import syscall, algorithm
from .algorithm import PWW

__version__ = "0.1.0a1"

def test_suite():
    """returns testsuite with all pww tests"""
    return unittest.TestSuite([syscall.test_suite()])
