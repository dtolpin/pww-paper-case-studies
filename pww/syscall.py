import re
import random
import time
import numpy as np
import unittest

class Syscall:
    """system call"""
    LINE = re.compile("^([^\s]+)(.*?)(?:\s*=>\s*(.*))?$")
    ARG = re.compile("([^\s=]+)=([^\s]*)")
    
    def __init__(self, line):
        self.name, args, self.ret = Syscall.LINE.match(line).groups()
        self.args = Syscall.ARG.findall(args)
        
    def __str__(self):
        """human-readable representation of the system call"""
        args = "".join(" %s=%s" % (k, v) for k, v in self.args)
        s = self.name
        if self.args:
            s += "".join(" %s=%s" % (k, v) for k, v in self.args)
        if self.ret:
            s += " => " + self.ret
        return s
    
    def __repr__(self):
        """repr is a string literal suitable for passing to Syscall"""
        return repr(str(self))   


def gen_shell_code():
    """generative model for the system calls"""
    x = random.randint(3, 127)
    y = random.randint(x + 1, 128)
    z = random.choice(["sh", "csh", "ksh"])
    fds = [0, 1, 2]
    random.shuffle(fds)
    return ([Syscall("accept fd=%s => %s" % (x, y))] +
            [Syscall("dup fd=%s => %s" % (y, fd)) for fd in fds] +
            [Syscall("execve sh=%s" % z)])


def search_shell_code(window, pause=1E-7):
    """searches for the shell code in a window"""
    accepted_ports = {}
    for syscall in window:
        # make it artificially slow
        # to estimate amount of work
        time.sleep(pause) 
        if syscall.name == 'accept':
            accepted_ports[syscall.ret] = set()
        elif syscall.name == 'dup':
            fd = None
            for name, value in syscall.args:
                if  name == 'fd':
                    if value in accepted_ports:
                        accepted_ports[value].add(syscall.ret)
        elif syscall.name == 'execve':
            for duped in accepted_ports.values():
                if duped >= {"0", "1", "2"}:
                    return True
    return False


def inject_shell_code(background, duration):
    """Injects a random shellcode into the background,
    with given shellcode duration. 
    Returns the patched background, and the first position 
    after the shellcode.
    """
    
    shellcode = gen_shell_code()
    first_index = np.random.randint(len(background) - duration) \
        if len(background) > duration \
        else 0
    last_index = first_index + duration - 1
    indices = sorted(np.random.choice(range(first_index+1, last_index),
                                      len(shellcode) - 2,
                                      replace=False))
    patch = background[first_index:last_index + 1]
    patch[0] = shellcode[0]
    patch[-1] = shellcode[-1]
    for i in range(1, len(shellcode) - 1):
        patch[indices[i - 1] - first_index] = shellcode[i]
    stream = background[:first_index] + patch + background[last_index + 1:]
    return stream, last_index + 1

class TestSyscall(unittest.TestCase):
    """Test case for syscall parser"""

    def test_nameonly(self):
        """parsing syscalls without arguments or result"""
        sc = Syscall("exit")
        self.assertEqual(sc.name, "exit")
        self.assertEqual(sc.args, [])
        self.assertEqual(sc.ret, None)

    def test_args(self):
        """parsing syscalls with arguments"""
        sc = Syscall("recvfrom fd=1 nbytes=10")
        self.assertEqual(sc.name, "recvfrom")
        self.assertEqual(sc.args, [("fd", "1"), ("nbytes", "10")])
        sc = Syscall("write fd=1 data=a=b")
        self.assertEqual(sc.name, "write")
        self.assertEqual(sc.args, [("fd", "1"), ("data", "a=b")])

    def test_result(self):
        """parsing syscalls with result"""
        sc = Syscall("select => 1")
        self.assertEqual(sc.name, "select")
        self.assertEqual(sc.args, [])
        self.assertEqual(sc.ret, "1")

    def test_args_result(self):
        """parsing syscall with args and result"""
        sc = Syscall("read fd=6 size=24 => 24")
        self.assertEqual(sc.name, "read")
        self.assertEqual(sc.args, [("fd", "6"), ("size", "24")])
        self.assertEqual(sc.ret, "24")


class TestShellCode(unittest.TestCase):
    """test case for shell code generation and detection"""

    def test_gen_shell_code(self):
        """testing shell code generator."""
        self.assertEqual(len(gen_shell_code()), 5)

    def test_search_shell_code(self):
        self.assertFalse(search_shell_code([]))
        self.assertTrue(search_shell_code(gen_shell_code()))
        self.assertTrue(search_shell_code([Syscall("wait")]+gen_shell_code()))

    def test_inject_shell_code(self):
        """testing shell code injection"""
        background = [Syscall("wait")] * 10

        stream, pos = inject_shell_code(background, 7)
        self.assertEqual(len(stream), len(background))
        self.assertTrue(pos >= 7)
        self.assertTrue(search_shell_code(stream))

        stream, pos = inject_shell_code(background, 5)
        self.assertEqual(len(stream), len(background))
        self.assertTrue(pos >= 5)
        self.assertTrue(search_shell_code(stream))

        stream, pos = inject_shell_code(background, 10)
        self.assertEqual(len(stream), len(background))
        self.assertTrue(pos == 10)
        self.assertTrue(search_shell_code(stream))

        
def test_suite():
    """returns testsuite with tests for the syscall parser"""
    return unittest.TestSuite([TestSyscall(test)
                               for test in ["test_nameonly",
                                            "test_args",
                                            "test_result",
                                            "test_args_result"]] +
                              [TestShellCode(test)
                               for test in ["test_gen_shell_code",
                                            "test_search_shell_code",
                                            "test_inject_shell_code"]])
