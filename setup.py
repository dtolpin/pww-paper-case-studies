from setuptools import setup, find_packages
import pww

setup(
    name="pww",
    version=pww.__version__,
    packages=find_packages(),

    # source code layout
    test_suite="pww.test_suite",

    # author and license
    author="David Tolpin",
    author_email="david.tolpin@gmail.com",
    description="PWW case studies",
)
