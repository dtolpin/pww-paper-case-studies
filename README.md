# Case studies for paper 'Progressive Temporal Window Widening'

The case studies are organized as Jupyter (IPython) notebooks, 
in folder `notebooks/`. The code in the notebooks relies on
the library modules provided by `pww` package, also in this
repository. 

To view the case studies.

* Create a virtual environment (optional but recommended).
* Install the library modules (`python setup.py install`).
* Start Jupyter notebook environment (`jupyter notebook`).
* Select a notebook to view.
